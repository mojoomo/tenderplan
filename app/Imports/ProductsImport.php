<?php

namespace App\Imports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
//use Maatwebsite\Excel\Concerns\WithHeadingRow;
//use Maatwebsite\Excel\Concerns\WithColumnFormatting;
//use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Throwable;
class ProductsImport implements ToModel, SkipsOnError, WithValidation, SkipsOnFailure, WithStartRow
    //, WithColumnFormatting
{
//, WithMapping
    use Importable, SkipsErrors, SkipsFailures;

    public function startRow(): int
    {
        return 2;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        return new Product([
            //
          'code' => "$row[0]",
          'title' => "$row[1]",
          'unit_of_measure' => "$row[2]", // ed. ismereniya
          'price_shipment_from_50_th_rub' => "$row[3]",
          'price_shipment_from_100_th_rub' => "$row[4]",
          'manufacturer' => "$row[5]",
          'origin_country' => "$row[6]",
          'characteristics' => "$row[7]",
          'count' => "$row[8]",
          'description' => "$row[9]",
          'articul' => "$row[10]",
          'quantity_per_pack_pieces' => "$row[11]",
          'color' => "$row[12]",
          'price_per_package' => "$row[13]",
          'multiplicity_in_the_tr_box' => "$row[14]", // kratnost v tr upakovke
          'packing_norms' => "$row[15]",
          'discount' => "$row[16]",
          'price_without_discount' => "$row[17]",
          'price_min' => "$row[18]",
          'discount_of_price_min' => "$row[19]",
          'lot_N' => "$row[20]",
          'remains' => "$row[21]",
          'expiration_date_percent' => "$row[22]",
          'expiration_date' => "$row[23]",
          'series' => "$row[24]",
          'wholesale' => "$row[25]", // OPT
          'nn' => "$row[26]", // nn
          'user_id' => Auth::id()
        ]);
    }

//    public function onError(Throwable $error) {
//    }

//    public function map($invoice): array {
//        return [
//            $invoice->invoice_number,
//            Date::dateTimeToExcel($invoice->created_at),
//            $invoice->total
//        ];
//    }

//    public function columnFormats(): array
//    {
//        return [
//            'B' => NumberFormat::FORMAT_TEXT,//FORMAT_DATE_DDMMYYYY,
//            'C' => NumberFormat::FORMAT_TEXT//FORMAT_CURRENCY_EUR_SIMPLE,
//        ];
//    }


    public function rules(): array {
        //numeric|min:1|max:50
        return [
            '0' => 'required',
            '1' => 'required',
            '2' => 'required',
            '3' => 'required',
            '4' => 'required',
            '5' => 'required',
            '6' => 'required',
            '7' => 'required',
            '8' => 'required',
            '9' => 'required'
        ];
    }
//    public function customValidationAttributes() {
//        return [];
//    }

//    public function onFailure(Failure ...$failure) {
//
//    }
}
