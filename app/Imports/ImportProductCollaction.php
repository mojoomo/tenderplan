<?php

namespace App\Imports;

use Illuminate\Support\Collection;
//use Maatwebsite\Excel\Concerns\SkipsErrors;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
/*

use Maatwebsite\Excel\Concerns\SkipsOnError;

use Maatwebsite\Excel\Concerns\WithStartRow;
*/

class ImportProductCollaction implements ToCollection, WithStartRow, SkipsOnFailure
{
    use SkipsFailures;

    public function startRow(): int
    {
        return 2;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {

//        Validator::make($rows->toArray(), [
////            '*.0' => 'required',
//               9 => 'required|unique:products'
//        ])->validate();
        //

        foreach ($rows as $key => $row) {
            if(!$row->filter()->isEmpty()) {

                if($row[$key] === '' || $row[$key] === null) {
                    continue;
                }

                Product::insert([
                    'code' => $row[0], //?? 0
                    'unit_of_measure' => $row[2] , // ed. ismereniya //?? 0
                    'product_title' => $row[1], //?? 0
                    'price_shipment_from_50_th_rub' => $row[3] , //?? 0
                    'price_shipment_from_100_th_rub' => $row[4] , //?? 0
                    'manufacturer' => $row[5] , //?? 0
                    'origin_country' => $row[6] , //?? 0
                    'characteristics' => $row[7] , //?? 0
                    'count' => $row[8] , //?? 0
                    'description' => $row[9] , //?? 0
                    'articul' => $row[10]  , //?? 0
                    'quantity_per_pack_pieces' => $row[11] , //?? 0
                    'color' => $row[12] , //?? 0
                    'price_per_package' => $row[13] , //?? 0
                    'multiplicity_in_the_tr_box' => $row[14] , // kratnost v tr upakovke //?? 0
                    'packing_norms' => $row[15] , //?? 0
                    'discount' => $row[16] , //?? 0
                    'price_without_discount' => $row[17] , //?? 0
                    'price_min' => $row[18] , //?? 0
                    'discount_of_price_min' => $row[19] , //?? 0
                    'lot_N' => $row[20] , //?? 0
                    'remains' => $row[21] , //?? 0
                    'expiration_date_percent' => $row[22] , //?? 0
                    'expiration_date' => $row[23] , //?? 0
                    'series' => $row[24] , //?? 0
                    'wholesale' => $row[25] , // OPT //?? 0
                    'nn' => $row[26] , // nn //?? 0
                    'sizes' => $row[27],
                    'user_id' => Auth::user()->id
                ]);
            }

        }

    }
}
