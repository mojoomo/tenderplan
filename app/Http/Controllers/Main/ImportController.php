<?php

namespace App\Http\Controllers\Main;

use App\Imports\ImportProductCollaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;



class ImportController extends Controller
{

    public function importing(Request $request) {
        try {
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 120000);

            $valid = Validator::make($request->all(), [
                'products_file' => 'required|mimes:xlsx'
            ]);
            if ($valid->fails()) {
                return redirect()->back()->withErrors($valid)->withInput($request->input());
            }
//            $file = $request->file('products_file');//->store();
            $import = new ImportProductCollaction;//new ProductsImport;
            Excel::import($import, $request->file('products_file'));

        }
        catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();

            return view('user.profile', compact('failures'));
        }
        return back()->with(["successUpload" => "Successful Upload"]);


    }
}
