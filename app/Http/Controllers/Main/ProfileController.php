<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //
    public function index() {
        return view('site.profile.index');
    }

    public function sellerIndex() {
        return view('site.sellerProfile.index');
    }


}
