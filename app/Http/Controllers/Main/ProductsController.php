<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    //

    public function userProducts(Request $request, $id) {
        return Product::where('user_id', $id)->get();
    }

    public function searchProduct(Request $request) {

        $v = Validator::make($request->all(), [
            'search' => 'required'
        ]);

        if($v->fails()){
            return back()->withErrors($v->errors()->getMessages());
        }

        $q = $request->get('search');
        if(is_string($q) || is_int($q)) {
            $product = Product::where("product_title", 'LIKE', '%'. $q .'%')
                ->orWhere('characteristics', 'LIKE', '%'.$q.'%')
                ->orWhere('description', 'LIKE', '%'.$q.'%')
                ->orWhere("price_without_discount", '=', $q)
                ->orWhere("price_without_discount", '=', $q + 10)
                ->orWhere("price_without_discount", '=', $q - 10)
                ->get();
            if(count($product) > 0)
                return view('site.profile.index')->withDetails($product)->withQuery ( $q );
            else
                return view('site.profile.index')->withMessage('Нет соответсвующих полей, попробуйте снова');
        }



    }

}
