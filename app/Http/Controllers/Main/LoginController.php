<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Role;
use App\Models\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;



class LoginController extends Controller
{
    //
    public function loginShow(Request $request) {

        return view('auth.login');

    }

    public function signin(Request $request) {

        $loginValid = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        if($loginValid->fails())
            return redirect()->back()->withErrors($loginValid->errors()->getMessages());


        $user = User::where('email', $request["email"])->first();

        if(!$user) return Redirect::back()->withInput()->withFlashMessage('Unknown username.');

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password]) ) {
//            if(User::where('id', Auth::id())->is("seller")) {
//                dd("works seller");
//            }
//
//            if(User::where('id', Auth::id())->is("user")) {
//                dd("works user");
//            }
            if(auth()->user()->is('user'))
                return redirect()->intended(route('user.profile'));

            if(auth()->user()->is('seller'))
                return redirect()->intended(route('user.seller_profile'));

        }

        return redirect()->back()->with(["wrong_login" => "Wrong username/password combination."]);

    }

    public function signupShow() {
        $roles = Role::query()->where('id', '!=', 3)->get();
        return view("auth.register", compact("roles"));
    }

    public function signup(Request $request) {

        $valid = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'role' => 'required',
        ]);

        if($valid->fails())
            return redirect()->back()->withErrors($valid->errors()->getMessages());

        if(isset($request["role"]) && !empty($request["role"]) && $request["role"] == 3)
            return redirect()->route('home');

        $user = new User();
        $user->name = $request["name"];
        $user->email = $request["email"];
        $user->password = bcrypt($request["password"]);

        $user->save();
    
        if(isset($request["role"]) && !empty($request["role"]) && $request["role"] == 1)
            $user->assignRole('user');
        if(isset($request["role"]) && !empty($request["role"]) && $request["role"] == 2)
            $user->assignRole('seller');

//        Auth::login($user);

        return redirect()->route('signinShow');

    }



}
