<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\User;

class PagesController extends Controller
{



    public function getAllUsers() {
        $users = User::with('roles')->whereHas(
            'roles', function($q){
            $q->where('name', '!=', 'admin');
        })->get();

        return $users;
    }

    public function getAllProducts() {
        $products = Product::all();
        return $products;
    }

}
