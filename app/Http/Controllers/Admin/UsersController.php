<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.dashboard.users.index');

    }


    public function create(Request $request) {

    }

    public function strore(Request $request ) {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {
        //
            if($request->ajax()) {
                $name = $request->get('name');
                $email = $request->get('email');

                $ok = User::where('id', $user)->update([
                   'name' => $name,
                   'email' => $email
                ]);

                if($ok) {
                    return response()->json(["success" => "ok"]);
                }
            }


    }


    public function show(Request $request, $user) {

        if($request->ajax()) {
            $user = User::where('id', $user)->first();
            return $user;
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()) {
            $user = User::destroy($id);
            if($user) {
                return response()->json(["success" => true]);
            }
        }

    }
}
