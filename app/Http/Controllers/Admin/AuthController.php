<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    //
    public function login(Request $request) {
        return view('admin.loginAdmin');
    }

    public function logout() {
        Session::flush();
        Auth::logout();
        return redirect()->route('home');
    }

    public function signIn(Request $request) {

        $authValid =  Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($authValid->fails()) {
            return back()->withErrors($authValid->errors()->getMessages());
        }

        if(Auth::attempt(['email' => $request->get("email"), 'password' => $request->get('password')]) && $request->user()->is('admin')) {
            return redirect()->intended(route('admin.users.index'));
        }

        return back()->with(["wrongAuth" => "Authentication wrong"]);

    }

}
