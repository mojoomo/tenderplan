<?php

namespace App\Http\Middleware;

use Closure;
use Fideloper\Proxy\TrustProxies as Middleware;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserMiddle extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

       if($request->user()) {
           $user = $request->user();
           if($user->is('admin') || !Auth::user() || Auth::guest()) { //|| Auth::user() && Auth::user()->roles->id === 3
               return redirect()->back();
           }else {
               return $next($request);
           }
       }
       return redirect()->back();



    }
}
