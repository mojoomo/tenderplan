<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Role;

class User extends Authenticatable
{
    use Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'address', 'company' //'bonus','balance'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $table = "users";

    public function roles() {
       return $this->belongsToMany('\App\Models\Role', "user_roles","user_id", "role_id")->withTimestamps();
    }

    public function is($roleName) {
//        foreach ($this->roles()->get() as $role)
//        {
//            if ($role->name == $roleName)
//            {
//                return true;
//            }
//        }
//
//        return false;
       return $this->roles->pluck( 'name' )->contains($roleName);
    }

    public function assignRole($role) {
        //return $this->roles()->sync(Role::whereName($role)->firstOrFail());
        return $this->roles()->attach( Role::where("name", '=', $role)->firstOrFail()->id);
//        return $this->roles()->sync( Role::whereName($role)->take(1)->pluck('id')->toArray(), false);
    }

    public function products() {
        return $this->hasMany('\App\Models\Product', 'user_id', 'id');
    }

}
