<?php

if(!function_exists('returnNotEmpty')) {
    function returnNotEmpty($array){
        return array_filter($array, function($i){
            return !empty($i);
        });
    }

}
