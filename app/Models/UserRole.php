<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    //
    protected $fillable = ['role_id', 'user_id'];
    protected $table = "user_roles";

    public function user() {

    }

    public function role() {

    }
}
