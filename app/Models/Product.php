<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = [
        'code',
        'unit_of_measure',
        'price_shipment_from_50_th_rub',
        'price_shipment_from_100_th_rub',
        'manufacturer',
        'origin_country',
        'characteristics',
        'count',
        'description',
        'articul',
        'quantity_per_pack_pieces',
        'color',
        'price_per_package',
        'multiplicity_in_the_tr_box',
        'packing_norms',
        'discount',
        'price_without_discount',
        'price_min',
        'discount_of_price_min',
        'lot_N',
        'remains',
        'expiration_date_percent',
        'expiration_date',
        'series',
        'wholesale',
        'nn',
        'user_id',
        ];

    public function user() {
        return $this->hasOne('App\User', 'user_id', 'id');
    }

}
