<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->string('product_title')->nullable();
            $table->string('unit_of_measure')->nullable(); // ed ismereniya
            $table->float('price_shipment_from_50_th_rub')->nullable();
            $table->float('price_shipment_from_100_th_rub')->nullable();
            $table->string('manufacturer')->nullable();
            $table->string('origin_country')->nullable();
            $table->string('characteristics')->nullable();
            $table->integer('count')->nullable();
            $table->string('description')->nullable();
            $table->string('articul')->unique()->nullable();
            $table->float('quantity_per_pack_pieces')->nullable();
            $table->string('color')->nullable();
            $table->float('price_per_package')->nullable();
            $table->integer('multiplicity_in_the_tr_box')->nullable(); // kratnost v tr upakovke
            $table->string('packing_norms')->nullable();
            $table->boolean('discount')->nullable();
            $table->float('price_without_discount')->nullable();
            $table->float('price_min')->nullable();
            $table->float('discount_of_price_min')->nullable();
            $table->string('lot_N')->nullable();
            $table->string('remains')->nullable();
            $table->float('expiration_date_percent')->nullable();
            $table->string('expiration_date')->nullable();
            $table->string('series')->nullable();
            $table->float('wholesale')->nullable(); // OPT
            $table->string('nn')->nullable(); // nn
            $table->string('sizes')->nullable(); // nn
            $table->unsignedBigInteger('user_id')->default(1);
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
