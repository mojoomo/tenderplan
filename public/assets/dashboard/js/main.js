// $(function () {
//     $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });

    $("#adminProductsList").bootstrapTable(
        {
            method: "POST",
            url: adminProductsUrl,
            pageSize: localStorage.getItem("page") ? localStorage.getItem("page") : 10,
            pagination: true,
            search: true,
            showColumns: true,
            cache: false,
            striped: false,
            ajaxOptions: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            showPaginationSwitch: true,
            showRefresh: true,
            cookie: true,
            export: true,
            showExport: true,
            clickToSelect: true,
            toolbar: "#toolbar",
            showPrint: true,
            resizable: true,
            columns: [
                {
                    title: "Управлять",
                    field: "id",
                    formatter: function (val, row) {
                        return "<button  data-toggle='modal' data-target='#usersUpdate' class='btn' onclick='update(" + row.id + ")'>Изменить</button>"
                    }
                },
                {
                    title: "Код",
                    field: "code",
                    sortable: true,
                    filterType: 'input',
                    filterControl: 'input',
                    formatter: function (val, row) {
                        return row.code
                    }
                },
                {
                    title: "Наименование",
                    field: "product_title",
                    sortable: true,
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.product_title
                    }
                },
                {

                    title: "Ед. Изм",
                    sortable: true,
                    sortName: 'unit_of_measure',
                    // field: "unque",
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.unit_of_measure
                    }
                },
                {

                    title: "Цена  при сумме  отгрузки от 50 тыс. руб",
                    sortable: true,
                    sortName: 'price_shipment_from_50_th_rub',
                    // field: "unque",
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.price_shipment_from_50_th_rub
                    }
                },
                {
                    title: "Цена  при сумме  отгрузки от 100 тыс. руб",
                    field: "price_shipment_from_100_th_rub",
                    sortable: true,
                    sortName: 'price_shipment_from_100_th_rub',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.price_shipment_from_100_th_rub
                    }
                },
                {
                    title: "Производитель",
                    // field: "roles",
                    sortable: true,
                    sortName: 'manufacturer',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.manufacturer;
                    }
                },
                {
                    title: "Страна происхождения",
                    field: "origin_country",
                    sortable: true,
                    sortName: 'origin_country',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.origin_country
                    }
                },{
                    title: "Характеристики товара",
                    field: "characteristics",
                    sortable: true,
                    sortName: 'characteristics',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.characteristics
                    }
                },{
                    title: "Количесво",
                    field: "count",
                    sortable: true,
                    sortName: 'count',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.count
                    }
                },{
                    title: "Описание",
                    field: "description",
                    sortable: true,
                    sortName: 'description',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.description
                    }
                },{
                    title: "Артикул",
                    field: "articul",
                    sortable: true,
                    sortName: 'articul',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.articul
                    }
                },{
                    title: "Кол-во в упак., шт",
                    field: "quantity_per_pack_pieces",
                    sortable: true,
                    sortName: 'quantity_per_pack_pieces',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.quantity_per_pack_pieces
                    }
                },{
                    title: "Цвет",
                    field: "color",
                    sortable: true,
                    sortName: 'color',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.color
                    }
                },{
                    title: "Цена за уп.",
                    field: "price_per_package",
                    sortable: true,
                    sortName: 'price_per_package',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.price_per_package
                    }
                },{
                    title: "Кратность в тр.коробке",
                    field: "multiplicity_in_the_tr_box",
                    sortable: true,
                    sortName: 'multiplicity_in_the_tr_box',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.multiplicity_in_the_tr_box
                    }
                },{
                    title: "норма уп.",
                    field: "packing_norms",
                    sortable: true,
                    sortName: 'packing_norms',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.packing_norms
                    }
                },{
                    title: "скидка",
                    field: "discount",
                    sortable: true,
                    sortName: 'discount',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.discount == 1 ? "Да" : ( row.discount == null ? "-" : "Нет");
                    }
                },{
                    title: "Цена без скидки",
                    field: "price_without_discount",
                    sortable: true,
                    sortName: 'price_without_discount',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.multiplicity_in_the_tr_box
                    }
                },{
                    title: "ЦМИН",
                    field: "price_min",
                    sortable: true,
                    sortName: 'price_min',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.price_min
                    }
                },{
                    title: "Скидка к ЦМИН",
                    field: "discount_of_price_min",
                    sortable: true,
                    sortName: 'discount_of_price_min',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.discount_of_price_min
                    }
                },{
                    title: "Лот №",
                    field: "lot_N",
                    sortable: true,
                    sortName: 'lot_N',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.lot_N
                    }
                },{
                    title: "Остаток",
                    field: "remains",
                    sortable: true,
                    sortName: 'remains',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.remains
                    }
                },{
                    title: "Срок годн.,%",
                    field: "expiration_date_percent",
                    sortable: true,
                    sortName: 'expiration_date_percent',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.expiration_date_percent
                    }
                },{
                    title: "Срок годности",
                    field: "expiration_date",
                    sortable: true,
                    sortName: 'expiration_date',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.expiration_date
                    }
                },{
                    title: "Серия",
                    field: "series",
                    sortable: true,
                    sortName: 'series',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.series
                    }
                },{
                    title: "ОПТ",
                    field: "wholesale",
                    sortable: true,
                    sortName: 'wholesale',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.wholesale
                    }
                },{
                    title: "нн",
                    field: "nn",
                    sortable: true,
                    sortName: 'nn',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.nn
                    }
                },{
                    title: "размеры",
                    field: "sizes",
                    sortable: true,
                    sortName: 'sizes',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.sizes
                    }
                },{
                    title: "Создан",
                    field: "created_at",
                    sortable: true,
                    sortName: 'created_at',
                    filterType: 'select',
                    filterControl: 'select',
                    formatter: function (val, row) {
                        return row.created_at
                    }
                },
                {
                    title: "Удалить",
                    formatter: function (val, row) {
                        return '<button type="button" class="pd-setting-ed deleteUser"  data-name="delete" title="Удалить" onclick="deleteUser(' + row.id + ')">' +
                            '<i class="fa fa-trash" aria-hidden="true" style="color: #CA0B00;"></i>' +
                            '</button>'
                    }
                },

            ]
        });
// });
