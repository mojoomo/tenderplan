<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

// User
Route::get("/signin", 'Main\LoginController@loginShow')->name('signinShow');
Route::post("/signin", 'Main\LoginController@signin')->name('signinPost');
Route::get("/signup", 'Main\LoginController@signupShow')->name('signupShow');
Route::post("/signup", 'Main\LoginController@signup')->name('signupPost');

Route::middleware("user_middle")->group(function() {
    Route::prefix("user")->name("user.")->group(function() {
        Route::get("/profile", "Main\ProfileController@index")->name("profile");
        Route::get("/seller-profile", "Main\ProfileController@sellerIndex")->name("seller_profile");
        Route::post('/importProducts', 'Main\ImportController@importing')->name("productsImporting");
        Route::post('/user-products/{id}', 'Main\ProductsController@userProducts')->name('userProducts');
        Route::get('/result', "Main\ProductsController@searchProduct")->name('search');
    });
});

// Admin
Route::get('admin/login', 'Admin\AuthController@login')->name('adminLogin');
Route::post('admin/signin', 'Admin\AuthController@signIn')->name('adminSignIn');
Route::get("admin/logout", "Admin\AuthController@logout")->name("logoutAdmin");

Route::middleware("admin_middle")->group(function() {
    Route::prefix("admin")->name('admin.')->group(function() {
        Route::resource("users", "Admin\UsersController", ['only' => ['index', 'create', 'update', 'destroy', 'store', 'show']]);//->except(['show', 'store', 'edit']);
        Route::resource("products", "Admin\ProductsController", ['only' => ['index', 'create', 'update', 'destroy', 'store', 'show']]);

        Route::post("users/getAll", "Admin\PagesController@getAllUsers")->name('allUsers');
        Route::post("products/getAll", "Admin\PagesController@getAllProducts")->name('allProducts');
    });
});


Auth::routes();
