@extends('site.extentions.profile-layout')

@section('seller_profile_index')
    {{--{{ isset($failures) ? dd($failures) : "not variable" }}--}}
    <h1>Seller</h1>

    @if(Session::has("successUpload"))
        <b>{{ Session::get('successUpload') }}</b>
    @endif
    <section class="import">
        <form action="{{ route('user.productsImporting') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="file" name="products_file" class="form-control-file">
            <button>Import</button>
        </form>
    </section>

    <section class="products_list">
        <table id="userProductsList" data-toggle='table' data-filter-control="true"></table>

    </section>
@endsection

@section('script')
    @php $user = \Illuminate\Support\Facades\Auth::user(); @endphp
    <script>

        let current_owner = "{{ $user->id }}";
        let productsUrl = "{{ route('user.userProducts', ':id') }}";
        productsUrl = productsUrl.replace(':id', current_owner);
        // const App = new App();
        App.sellerProducts()
    </script>
@endsection