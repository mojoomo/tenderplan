@extends('site.extentions.profile-layout')
@section('profile_index')
{{--{{ isset($failures) ? dd($failures) : "not variable" }}--}}
    <h1>Buyer</h1>
{{--    <a class="dropdown-item" href="{{ route('logout') }}"--}}
{{--       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">--}}
{{--        {{ __('Logout') }}--}}
{{--    </a>--}}
{{--    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
{{--        @csrf--}}
{{--    </form>--}}
    @if(Session::has("successUpload"))
        <b>{{ Session::get('successUpload')  }}</b>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p><b>Критерии</b><br>
                    <ul>
                        <li>цена</li>
                        <li>хорактеристики</li>
                        <li>Имя</li>
                        <li>описания</li>
                    </ul>
                </p>
                <form action="{{ route('user.search') }}" method="GET">
                    <div class="fomr-group">
                        <input type="text" placeholder="Поиск товаров" name="search" class="input-cotrol">
                        <button class="btn">Искать</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            @if(isset($details))
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Имя</th>
                        <th>хорактеристики</th>
                        <th>цена</th>
                        <th>описания</th>
                        <th>Единица Измерения</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($details as $user)
                        <tr>
                            <td>{{$user->product_title}}</td>
                            <td>{{$user->characteristics}}</td>
                            <td>{{$user->price_without_discount}}</td>
                            <td>{{$user->description}}</td>
                            <td>{{$user->unit_of_measure}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
            @if(isset($message))
                <p>{{ $message }}</p>
            @endif
        </div>
    </div>


@endsection

@section('script')
    @php $user = \Illuminate\Support\Facades\Auth::user(); @endphp
    <script>
        $.ajax({
            url: '',
            type: 'get',
            cache: false,
            data: {},
            success: function(resp) {
                console.log("res");
            },
        }).fail(function(err, status, error){});
    </script>
@endsection
