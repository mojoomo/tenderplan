@extends('layouts.main')

@section('profile-content')

    @if(auth()->user()->is('user'))
        @yield('profile_index')
    @endif

    @if(auth()->user()->is('seller'))
        @yield('seller_profile_index')
    @endif

@endsection