<!doctype html>
<html lang="en"> {{--{{ str_replace('_', '-', app()->getLocale()) }}--}}
<head>
    <meta charset="UTF-8">
{{--    <meta name="viewport"--}}
{{--          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">--}}
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="{{ asset('assets/dashboard/assets/css/fontawesome.all.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/dashboard/assets/css/now-ui-dashboard.css?v=1.5.0') }}" rel="stylesheet" />
    {{-- DataTable --}}
{{--    <link rel="stylesheet" href="{{ asset('assets/dashboard/datatable/css/bootstrap.min.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('assets/dashboard/assets/css/data-table/bootstrap-editable.css')  }}">
    <link rel="stylesheet" href="{{ asset('assets/dashboard/assets/css/data-table/bootstrap-table.css')  }}">
    <link rel="stylesheet" href="{{ asset('assets/dashboard/assets/css/sweetalert2.min.css')  }}">
{{--    <link rel="stylesheet" href="{{ asset('assets/dashboard/assets/js/jquery.min.js')  }}">--}}

    <style>
        .bootstrap-table {
            width: 100%;
        }
    </style>
    <title>@yield('adminTitle')</title>
</head>
<body>
    <div class="wrapper">

        @include('admin.includes.sidebar')
        @yield('adminContent')

    </div>

    <script type="text/javascript" src="{{ asset('assets/dashboard/assets/js/core/jquery.min.js')  }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        let adminProductsUrl = "{{ route('admin.allProducts') }}";
    </script>
    <script src="{{ asset('assets/dashboard/assets/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('assets/dashboard/assets/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/dashboard/assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
    <!-- Chart JS -->
{{--    <script src="{{ asset('assets/dashboard/assets/js/plugins/chartjs.min.js') }}"></script>--}}
    <!--  Notifications Plugin    -->
    <script src="{{ asset('assets/dashboard/assets/js/plugins/bootstrap-notify.js') }}"></script>
    <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('assets/dashboard/assets/js/now-ui-dashboard.min.js?v=1.5.0') }}" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->

    <script src='{{ asset('assets/dashboard/assets/js/jquery-ui.min.js') }}' type="text/javascript"></script>
{{--    <script  src="{{ asset('assets/dashboard/assets/js/sweetalert2.all.min.js') }}" />--}}
    <script src='{{ asset('assets/dashboard/assets/vendors/sweetalert2.js') }}' type="text/javascript"></script>
{{--    <script src='{{ asset('assets/dashboard/datatable/js/bootstrap.min.js') }}' type="text/javascript"></script>--}}
    <script src='{{ asset('assets/dashboard/assets/js/bootstrap-table.js') }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/tableExport.js')  }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/data-table-active.js') }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/bootstrap-table-editable.js')  }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/bootstrap-editable.js') }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/bootstrap-table-resizable.js')  }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/colResizable-1.5.source.js')  }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/bootstrap-table-export.js')  }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/js/main.js')  }}' type="text/javascript"></script>
{{--    <script  src="{{ asset('assets/dashboard/assets/js/sweetalert2.all.min.js') }}" />--}}
    @yield("admin_script")
</body>
</html>