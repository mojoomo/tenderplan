<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    {{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('assets/dashboard/assets/css/fontawesome.all.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/dashboard/assets/css/data-table/bootstrap-editable.css')  }}">
    <link rel="stylesheet" href="{{ asset('assets/dashboard/assets/css/data-table/bootstrap-table.css')  }}">
    <link rel="stylesheet" href="{{ asset('assets/dashboard/assets/css/sweetalert2.min.css')  }}">
    <style>
        .import {
            padding: 5px;
            border:1px solid black;
            border-radius:3px;
            margin: 15px auto;
            width: 379px;
        }
        .products_list {
            padding:10px;
        }
    </style>


    <title>Profile</title>
</head>
<body>

<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{--                    {{ config('app.name', 'App') }}--}}
                Home
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('signinShow') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('signup'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('signupShow') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">


        @auth
            @yield('profile-content')
        @else
            @yield('content')
        @endauth

    </main>
</div>

<script src="{{ asset('assets/all/js/jquery3.5.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/dashboard/assets/js/core/popper.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/assets/js/core/bootstrap.min.js') }}"></script>
<script src='{{ asset('assets/dashboard/assets/js/jquery-ui.min.js') }}' type="text/javascript"></script>
{{--<script src="{{ asset("assets/all/js/bootstrap.min.js") }}" type="text/javascript" async></script>--}}
<script src="{{ asset('assets/all/js/additional-methods.min.js') }}" type="text/javascript" async></script>
<script src="{{ asset('assets/all/js/jquery.validate.min.js') }}" type="text/javascript"></script>
@auth
    <script src="{{ asset('assets/dashboard/assets/js/plugins/bootstrap-notify.js') }}"></script>
    <script src='{{ asset('assets/dashboard/assets/js/bootstrap-table.js') }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/tableExport.js')  }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/data-table-active.js') }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/bootstrap-table-editable.js')  }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/bootstrap-editable.js') }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/bootstrap-table-resizable.js')  }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/colResizable-1.5.source.js')  }}' type="text/javascript"></script>
    <script src='{{ asset('assets/dashboard/assets/js/bootstrap-table-export.js')  }}' type="text/javascript"></script>
@endauth
<script src="{{ asset('assets/site/js/app.js') }}"></script>
@yield('script')
</body>
</html>