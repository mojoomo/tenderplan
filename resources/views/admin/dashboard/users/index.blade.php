@extends('layouts.admin')

@section('adminTitle')
    Users
@endsection


@section('adminContent')
        <div class="main-panel" id="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        <a class="navbar-brand" href="javascript:void(0);">@yield('adminTitle')</a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <form>
                            <div class="input-group no-border">
                                <input type="text" value="" class="form-control" placeholder="Search...">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="now-ui-icons ui-1_zoom-bold"></i>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="#pablo">
                                    <i class="now-ui-icons media-2_sound-wave"></i>
                                    <p>
                                        <span class="d-lg-none d-md-block">Stats</span>
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="now-ui-icons location_world"></i>
                                    <p>
                                        <span class="d-lg-none d-md-block">Some Actions</span>
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{ route('logoutAdmin') }}">Logout</a>
                                    {{--                                <a class="dropdown-item" href="#">Another action</a>--}}
                                    {{--                                <a class="dropdown-item" href="#">Something else here</a>--}}
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0);">
                                    <i class="now-ui-icons users_single-02"></i>
                                    <p>
                                        <span class="d-lg-none d-md-block">Account</span>
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            <div class="panel-header panel-header-sm">
            </div>
        <div class="content">

            <div class="row">
                <table id="usersData" data-toggle='table' data-filter-control="true"></table>
            </div>

        </div>
{{--            <div class="row">--}}
{{--                <div class="col-md-8">--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-header">--}}
{{--                            <h5 class="title">Edit Profile</h5>--}}
{{--                        </div>--}}
{{--                        <div class="card-body">--}}
{{--                            <form>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-5 pr-1">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Company (disabled)</label>--}}
{{--                                            <input type="text" class="form-control" disabled="" placeholder="Company" value="Creative Code Inc.">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-3 px-1">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Username</label>--}}
{{--                                            <input type="text" class="form-control" placeholder="Username" value="michael23">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-4 pl-1">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="exampleInputEmail1">Email address</label>--}}
{{--                                            <input type="email" class="form-control" placeholder="Email">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-6 pr-1">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>First Name</label>--}}
{{--                                            <input type="text" class="form-control" placeholder="Company" value="Mike">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-6 pl-1">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Last Name</label>--}}
{{--                                            <input type="text" class="form-control" placeholder="Last Name" value="Andrew">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Address</label>--}}
{{--                                            <input type="text" class="form-control" placeholder="Home Address" value="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-4 pr-1">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>City</label>--}}
{{--                                            <input type="text" class="form-control" placeholder="City" value="Mike">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-4 px-1">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Country</label>--}}
{{--                                            <input type="text" class="form-control" placeholder="Country" value="Andrew">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-4 pl-1">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Postal Code</label>--}}
{{--                                            <input type="number" class="form-control" placeholder="ZIP Code">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>About Me</label>--}}
{{--                                            <textarea rows="4" cols="80" class="form-control" placeholder="Here can be your description" value="Mike">Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</textarea>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-4">--}}
{{--                    <div class="card card-user">--}}
{{--                        <div class="image">--}}
{{--                            <img src="../assets/img/bg5.jpg" alt="...">--}}
{{--                        </div>--}}
{{--                        <div class="card-body">--}}
{{--                            <div class="author">--}}
{{--                                <a href="#">--}}
{{--                                    <img class="avatar border-gray" src="../assets/img/mike.jpg" alt="...">--}}
{{--                                    <h5 class="title">Mike Andrew</h5>--}}
{{--                                </a>--}}
{{--                                <p class="description">--}}
{{--                                    michael24--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                            <p class="description text-center">--}}
{{--                                "Lamborghini Mercy <br>--}}
{{--                                Your chick she so thirsty <br>--}}
{{--                                I'm in that two seat Lambo"--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                        <hr>--}}
{{--                        <div class="button-container">--}}
{{--                            <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">--}}
{{--                                <i class="fab fa-facebook-f"></i>--}}
{{--                            </button>--}}
{{--                            <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">--}}
{{--                                <i class="fab fa-twitter"></i>--}}
{{--                            </button>--}}
{{--                            <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">--}}
{{--                                <i class="fab fa-google-plus-g"></i>--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}




            <div class="modal fade" id="usersUpdate" tabindex="-1" role="dialog" aria-labelledby="usersUpdate" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="usersUpdate">Update users</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="name_user"> Name </label>
                                <input type="text" id="name_user" class="form-control name_user" name="name_user">
                            </div>
                            <div class="form-group">
                                <label for="email_user">Email</label>
                                <input type="text" id="email_user" class="form-control email_user" name="email_user">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn " data-dismiss="modal">Close</button>
                            <button type="button" class="btn update_save_user" >Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
@endsection

@section('admin_script')
    <script>
        $("#usersData").bootstrapTable(
            {
                method: "POST",
                url: '{{ route('admin.allUsers') }}',
                pageSize: localStorage.getItem("page") ? localStorage.getItem("page") : 10,
                pagination: true,
                search: true,
                showColumns: true,
                cache: false,
                striped: false,
                ajaxOptions: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                showPaginationSwitch: true,
                showRefresh: true,
                cookie: true,
                export: true,
                showExport: true,
                clickToSelect: true,
                toolbar: "#toolbar",
                showPrint: true,
                resizable: true,
                columns: [
                    {
                        title: "Управлять",
                        field: "id",
                        formatter: function (val, row) {
                            return "<button  data-toggle='modal' data-target='#usersUpdate' class='btn' onclick='update(" + row.id + ")'>Изменить</button>"
                        }
                    },
                    {
                        title: "Имя",
                        field: "name",
                        sortable: true,
                        filterType: 'input',
                        filterControl: 'input',
                        formatter: function (val, row) {
                            return row.name
                        }
                    },
                    {
                        title: "Эл. Почта",
                        field: "email",
                        sortable: true,
                        filterType: 'input',
                        filterControl: 'input',
                        formatter: function (val, row) {
                            return row.email
                        }
                    },
                    {

                        title: "Телефон",
                        sortable: true,
                        sortName: 'phone',
                        // field: "unque",
                        filterType: 'select',
                        filterControl: 'select',
                        formatter: function (val, row) {
                            return row.phone
                        }
                    },
                    {

                        title: "Аддресс",
                        sortable: true,
                        sortName: 'address',
                        // field: "unque",
                        filterType: 'select',
                        filterControl: 'select',
                        formatter: function (val, row) {
                            return row.address
                        }
                    },
                    // {
                    //     title: "Ակտիվ",
                    //     field: "Status",
                    //     sortable: true,
                    //     sortName: 'active',
                    //     filterType: 'select',
                    //     filterControl: 'select',
                    //     formatter: function (val, row) {
                    //         if(row.active == 1) {
                    //             return '<span class="label label-success">Ակտիվ</span>'
                    //         }else {
                    //             return '<span class="label label-danger">Պասիվ</span>'
                    //         }
                    //     }
                    // },
                    {
                        title: "Компания",
                        field: "company",
                        sortable: true,
                        sortName: 'comapny',
                        filterType: 'select',
                        filterControl: 'select',
                        formatter: function (val, row) {
                            return row.company
                        }
                    },
                    {
                        title: "Тип",
                        // field: "roles",
                        sortable: true,
                        sortName: 'Role.name',
                        filterType: 'select',
                        filterControl: 'select',
                        formatter: function (val, row) {
                            let role_name;
                            row.roles.forEach(function(item) {
                                role_name = item.name.split(',');
                            });
                            return role_name;

                        }
                    },
                    {
                        title: "Зарегистрирован",
                        field: "created_at",
                        sortable: true,
                        sortName: 'Role.name',
                        filterType: 'select',
                        filterControl: 'select',
                        formatter: function (val, row) {
                            return row.created_at
                        }
                    },
                    {
                        title: "Удалить",
                        formatter: function (val, row) {
                            return '<button type="button" class="pd-setting-ed deleteUser"  data-name="delete" title="Удалить" onclick="deleteUser(' + row.id + ')">' +
                                '<i class="fa fa-trash" aria-hidden="true" style="color: #CA0B00;"></i>' +
                                '</button>'
                        }
                    },

                ]
            });


        function update(user_id) {
            let url = "{{ route('admin.users.show', ':id') }}";
            url = url.replace(':id', user_id);
            $.ajax({
                url: url,
                type: 'get',
                cache: false,
                success: function (res) {
                    $(".modal-body input.name_user").val(res.name);
                    $(".modal-body input.email_user").val(res.email);
                    $('.modal button.update_save_user').attr('data-id', res.id);
                }
            });
        }


        $('.modal button.update_save_user').on("click touchstart", function(){
            let url = "{{ route('admin.users.update', ':id') }}";
            url = url.replace(':id', $(this).attr('data-id'));
            $.ajax({
               url: url,
               type: 'PUT',
               data: {
                   "name" : $(".modal-body input.name_user").val(),
                   "email" : $(".modal-body input.email_user").val()
               },
               cache: false,
               success: function(resp) {
                   if(resp.success == "ok") {
                       swal({
                           type: "success",
                           icon: "success",
                           text: 'Successfuly Updated',
                           button: 'Ok'
                       }).then(function(ok) {
                           if(ok) {
                             location.reload();
                           }
                       });

                    }
               }
            }).fail(function(err, status, error){
                console.log(error);
            });
        });

        function deleteUser(usr_id) {

            swal({
                title: 'Вы уверены ?',
                text: 'Удалить ?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Да удалить',
                cancelButtonText: 'Отменить',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false,
            }).then(() => {
                // $.param({"Id": Id, "bolDeleteReq" : bolDeleteReq}
                let url = "{{ route('admin.users.destroy', ':id') }}";
                url = url.replace(':id', usr_id); //$.param({"user":
                $.ajax({
                    url: url,
                    type: "DELETE",
                    cache: false,
                    success: function(resp) {
                        if(resp.success == true) {
                            swal({
                                title: 'Успешно удален !',
                                type: 'success',
                                icon: "success",
                                showConfirmButton: false,
                                timer: 2000
                            }).catch(swal.noop);
                        }
                    }
                });
            }, (dismiss) => {
                if (dismiss === 'concel') {

                }
            })

        }


    </script>




@endsection