<div class="sidebar" data-color="purple">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo">
        <a href="{{ route('home') }}" class="simple-text logo-mini">
{{--            Тендер--}}
        </a>
        <a href="{{ route('home') }}" class="simple-text logo-normal">
            Тендерплан
        </a>
    </div>
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
{{--            <li>--}}
{{--                <a href="./dashboard.html">--}}
{{--                    <i class="now-ui-icons design_app"></i>--}}
{{--                    <p>Dashboard</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="./icons.html">--}}
{{--                    <i class="now-ui-icons education_atom"></i>--}}
{{--                    <p>Icons</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="./map.html">--}}
{{--                    <i class="now-ui-icons location_map-big"></i>--}}
{{--                    <p>Maps</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="./notifications.html">--}}
{{--                    <i class="now-ui-icons ui-1_bell-53"></i>--}}
{{--                    <p>Notifications</p>--}}
{{--                </a>--}}
{{--            </li>--}}
            <li class="active ">
                <a href="{{ route('admin.users.index') }}">
                    <i class="now-ui-icons users_single-02"></i>
                    <p>Users Config</p>
                </a>
            </li>
            <li class="active">
                <a href="{{ route('admin.products.index') }}">
                    <i class="now-ui-icons shopping_shop"></i>
                    <p>Products</p>
                </a>
            </li>
{{--            <li>--}}
{{--                <a href="./tables.html">--}}
{{--                    <i class="now-ui-icons design_bullet-list-67"></i>--}}
{{--                    <p>Table List</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="./typography.html">--}}
{{--                    <i class="now-ui-icons text_caps-small"></i>--}}
{{--                    <p>Typography</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            --}}
        </ul>
    </div>
</div>